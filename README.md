# Slackware 14.2 Full-Disk Encryption

This guide describes the process of Slackware 14.2 installation with full-disk
LUKS encryption with an LVM and a bootable flash drive with a detached LUKS header
and an encrypted (or plain, if you prefer) keyfile.

If you plan to encrypt a keyfile, be sure to use a different algorithm, than the
one used for disk encryption.

Since AES is both an industry standard and is usually hardware-accelerated, it
has been chosen as a default disk encryption algorithm for the purposes of this guide.
However, it is highly recommended to consult the `cryptsetup benchmark` to make an
informed choice.

The included configs are intended as a starting point for your own configuration,
do *NOT* use them as-is.

## Preparing the flash drive

### EFI

Wipe the MBR/GPT data structures on the flash drive, create a new GPT table with a
disk-wide partition and format it as FAT.
```bash
sgdisk --zap-all /dev/sdX
sgdisk --largest-new=1 --typecode=1:EF00 /dev/sdX
mkfs.vfat -F 32 -n KEYDRIVE /dev/sdX1
```

Install rEFInd and copy a generic config.
```bash
mount /dev/sdX1 /mnt/tmp
mkdir -p /mnt/tmp/efi/boot
wget https://sourceforge.net/projects/refind/files/0.11.4/refind-bin-0.11.4.zip
unzip refind-bin-0.11.4.zip
cp refind-bin-0.11.4/refind/refind_x64.efi /mnt/tmp/efi/boot/bootx64.efi
cp slackware-fde-howto/configs/refind.conf /mnt/tmp/efi/boot
```

### MBR

Wipe the MBR/GPT data structures on the flash drive, create a new GPT table with a
disk-wide patition and format it as FAT.
```bash
sgdisk --zap-all /dev/sdX
dd if=/usr/share/syslinux/gptmbr.bin of=/dev/sdX bs=440 conv=notrunc count=1
sgdisk --largest-new=1 --typecode=1:EF00 /dev/sdX
sgdisk --attributes=1:set:2 /dev/sdX
mkfs.vfat -F 32 -n KEYDRIVE /dev/sdX1
```

Install Syslinux and copy a generic config.
```bash
syslinux --install /dev/sdX1
mount /dev/sdX1 /mnt/tmp
cp slackware-fde-howto/configs/syslinux.cfg /usr/share/syslinux/menu.c32 /mnt/tmp
```

### Finishing touches

Copy patches and `mksbkeys.sh` (if you plan to use EFI Secure Boot) and unmount
the flash drive.
```bash
cp -r slackware-fde-howto/{patches,scripts/mksbkeys.sh} /mnt/tmp
umount /mnt/tmp
```

## Installing Slackware

### Preparing the disk

Wipe the disk with a random data.
```bash
openssl enc -aes-256-ctr -pass pass:"$(dd if=/dev/urandom bs=128 count=1 2> /dev/null | tr -dc '[:graph:]')" -nosalt < /dev/zero | dd of=/dev/sdX status=progress bs=64K
```

Create a single disk-wide partition.
```bash
sgdisk --zap-all /dev/sdX
sgdisk --largest-new=1 --typecode=1:8E00 /dev/sdX
```

### Setting up LUKS

Mount the flash drive.
```bash
mkdir /keydrive
mount /dev/sdX1 /keydrive
```

#### Creating the encrypted keyfile (recommended)
```bash
dd if=/dev/zero of=/keydrive/key.luks bs=2M count=1
cryptsetup luksFormat --align-payload 1 --cipher twofish-xts-plain64 --hash sha512 --iter-time 10000 --key-size 512 /keydrive/key.luks
cryptsetup luksOpen /keydrive/key.luks lukskey
dd if=/dev/urandom of=/dev/mapper/lukskey
```

#### Creating the plain keyfile
```bash
dd if=/dev/urandom of=/keydrive/key.luks bs=2M count=1
```
If you're using a plain keyfile, you'd need to replace each instance of
`/dev/mapper/lukskey` with `/keydrive/key.luks` in the next section!

#### Creating the LUKS header and the container

Create the LUKS header file and create the LUKS container on the disk.
```bash
dd if=/dev/zero of=/keydrive/header.luks bs=2M count=1
cryptsetup luksFormat --cipher aes-xts-plain64 --hash sha512 --header /keydrive/header.luks --iter-time 10000 --key-file /dev/mapper/lukskey --key-size 512 /dev/sdX1
cryptsetup luksOpen --header /keydrive/header.luks --key-file /dev/mapper/lukskey /dev/sdX1 slackluks
```

Unmount the flash drive.
```bash
cryptsetup luksClose lukskey # If you're using an encrypted keyfile.
umount /keydrive
```

### Partitioning the disk and installing Slackware

Now you can follow `README_CRYPT.TXT` instructions on LVM setup and Slackware
installation. *Do NOT reboot after setup is finished!*
```
The LVM part is next.  Create a Physical Volume (PV) on device
'/dev/mapper/slackluks', a Volume Group (VG) called 'cryptvg' - any name will
do - on the PV, and three Logical Volumes (LV's) in the VG, one for your
root partition (7 GB in size), one for the /home partition (10 GB in size)
and a third which we will use for swap (1 GB in size).  You will probably
use different sizes depending on your environment and wishes, but keep the
sum of the LV sizes less than the total size of the Physical Volume:

  # pvcreate /dev/mapper/slackluks

  # vgcreate cryptvg /dev/mapper/slackluks

  # lvcreate -L 7G -n root cryptvg

  # lvcreate -L 10G -n home cryptvg

  # lvcreate -L 1G -n swap cryptvg

* Run 'mkswap' so that the 'setup' program will identify the 'swap'
LV as a valid swap partition:

  # mkswap /dev/cryptvg/swap

* With the system properly prepared, you can launch 'setup'.  The 'setup'
program will identify the LV swap volume and activate it.  When appointing
the partitions to use for your filesystems, select '/dev/cryptvg/root' for the
root partition, next select '/dev/cryptvg/home' to be used for your /home
```

### Setting up a bootable flash drive

Chroot into your new installation and mount the flash drive.
```bash
vgchange -ay
mount -o bind /dev /mnt/dev
mount -o bind /proc /mnt/proc
mount -o bind /sys /mnt/sys
chroot /mnt
mount /dev/sdX1 /mnt/tmp
```

Make necessesary changes to the initrd.
```bash
cd /boot
mkdir initrd-tree
pushd initrd-tree
tar --extract --file=/usr/share/mkinitrd/initrd-tree.tar.gz
patch -p0 < /mnt/tmp/patches/init-1.4.8.patch
echo "LABEL=KEYDRIVE:/header.luks" > luksheader
popd
```

Make the initrd and copy it along with the kernels to the flash drive. A list
of modules provided here should be adequate for most typical configurations,
but you should still consult `/usr/share/mkinitrd/mkinitrd_command_generator.sh`.
Also, if you're using the SMP kernel, you'll need to add `-smp` to the kernel
version and use the appropriate kernel images (i. e. `4.4.14-smp` and
`vmlinuz-{generic,huge}-smp`).
```bash
mkinitrd -C /dev/sdX1 -K LABEL=KEYDRIVE:/key.luks -L -f ext4 -h /dev/cryptvg/swap -r /dev/cryptvg/root -k 4.4.14 -m ext4:hid-generic:loop:usb-storage -o /boot/initrd.gz -u
cp vmlinuz-{generic,huge} initrd.gz -t /mnt/tmp
```

Unmount the flash drive, exit from the chroot, close the LUKS container and reboot.
```bash
umount /mnt/tmp
exit
umount /mnt/{dev,proc,sys,}
vgchange -an
cryptsetup luksClose slackluks
reboot
```

## Enabling EFI Secure Boot (optional)

Install `efitools` and `sbsign` (both are available at [SlackBuilds.org](https://slackbuilds.org/result/?search=secure+boot&sv=14.2)),
then use the included `mksbkeys.sh` script to generate the Secure Boot keys.
Installation of the keys is covered in the great detail in
[this article](http://www.rodsbooks.com/efi-bootloaders/controlling-sb.html)
by Roderick Smith.

After installing the keys, sign the boot manager and the kernel and copy them to
the flash drive.
```bash
sbsign --key DB.key --cert DB.crt --output /mnt/tmp/efi/boot/bootx64.efi /mnt/tmp/efi/boot/bootx64.efi
sbsign --key DB.key --cert DB.crt --output /mnt/tmp/vmlinuz-huge-signed /boot/vmlinuz-huge
sbsign --key DB.key --cert DB.crt --output /mnt/tmp/vmlinuz-generic-signed /boot/vmlinuz-generic
```

Reboot and turn on Secure Boot in UEFI. Everything should work now.

## Updating

Note that you'll need to rebuild `initrd`, re-sign the kernels (if you're using
Secure Boot) and copy the updated versions to the flash drive after *each* kernel
update! Also note that after updating `mkinitrd` package to the latest version
(1.4.10, at the time of this writing), you'll need to patch init with
`init-1.4.10.patch` instead of `init-1.4.8.patch`! Use `updkeydrive.sh`
as a template when writing an automation script for this task.

## Sources

* https://www.rodsbooks.com/efi-bootloaders/controlling-sb.html
* https://docs.slackware.com/howtos:security:enabling_secure_boot
* https://mirrors.slackware.com/slackware/slackware64-14.2/README_CRYPT.TXT
* https://wiki.archlinux.org/index.php/Dm-crypt/Device_encryption
* https://wiki.archlinux.org/index.php/Dm-crypt/Specialties
* https://wiki.archlinux.org/index.php/Securely_wipe_disk/Tips_and_tricks
* https://wiki.gentoo.org/wiki/Custom_Initramfs
* https://wiki.gentoo.org/wiki/DM-Crypt_LUKS
* https://wiki.gentoo.org/wiki/Syslinux

## License

Copyright © 2022 8B411

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.3
or any later version published by the Free Software Foundation;
with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.
A copy of the license is included in the file LICENSE.
