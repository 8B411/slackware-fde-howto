#!/bin/bash
# SPDX-License-Identifier: GPL-3.0-only
# SPDX-FileCopyrightText: 2015 Roderick W. Smith
# SPDX-FileContributor: Modified by 8B411 <8B411@disroot.org>

read -p "Enter a Common Name to embed in the keys: " -r name

openssl req -new -x509 -newkey rsa:4096 \
  -subj "/CN=${name:+$name }Platform Key/" \
  -keyout PK.key -out PK.crt -days 3650 -nodes -sha256
openssl req -new -x509 -newkey rsa:4096 \
  -subj "/CN=${name:+$name }Key Exchange Key/" \
  -keyout KEK.key -out KEK.crt -days 3650 -nodes -sha256
openssl req -new -x509 -newkey rsa:4096 \
  -subj "/CN=${name:+$name }Signature Database/" \
  -keyout DB.key -out DB.crt -days 3650 -nodes -sha256

openssl x509 -in PK.crt  -out PK.cer  -outform DER
openssl x509 -in KEK.crt -out KEK.cer -outform DER
openssl x509 -in DB.crt  -out DB.cer  -outform DER

guid=$(uuidgen --random | tee GUID.txt)
cert-to-efi-sig-list -g $guid PK.crt  PK.esl
cert-to-efi-sig-list -g $guid KEK.crt KEK.esl
cert-to-efi-sig-list -g $guid DB.crt  DB.esl
: > noPK.esl

date=$(date --date="1 second" --rfc-3339="seconds")
sign-efi-sig-list -t "$date" -k PK.key  -c PK.crt  PK  PK.esl   PK.auth
sign-efi-sig-list -t "$date" -k PK.key  -c PK.crt  PK  noPK.esl noPK.auth
sign-efi-sig-list -t "$date" -k PK.key  -c PK.crt  KEK KEK.esl  KEK.auth
sign-efi-sig-list -t "$date" -k KEK.key -c KEK.crt DB  DB.esl   DB.auth
