#!/bin/bash
# SPDX-License-Identifier: GPL-3.0+
# SPDX-FileCopyrightText: 2022 8B411 <8B411@disroot.org>

set -ex

partition=${partition:-LABEL=KEYDRIVE}
dbkey=${dbkey:-/boot/sbkeys/DB.key}
dbcrt=${dbcrt:-/boot/sbkeys/DB.crt}
patch=${patch:-/boot/init.patch}

# Try to guess kernel version if it was not provided
kernel=${kernel:-$(
  readlink -f /boot/vmlinuz | grep --only-matching "[0-9\.]\+[a-z\-]*"
)}

# Find the mountpoint of the keydrive
keydrive=${keydrive:-$(
  findmnt --first-only --noheadings --output target --raw --uniq \
    "$(blkid -l -o device -t $partition -c /dev/null)"
)}

# Patch ramdisk init to add detached header support
[[ -d /boot/initrd-tree ]] && rm -rf /boot/initrd-tree
mkdir /boot/initrd-tree && pushd /boot/initrd-tree
tar --extract --file=/usr/share/mkinitrd/initrd-tree.tar.gz
patch -p0 < $patch
echo "$partition:/header.luks" > luksheader
popd

# Create new ramdisk image and copy it to the keydrive
mkinitrd \
  -C /dev/sda1 \
  -K $partition:/key.luks \
  -L \
  -f ext4 \
  -h /dev/cryptvg/swap \
  -k $kernel \
  -m ext4:hid-generic:loop:usb-storage \
  -o /boot/initrd.gz \
  -r /dev/cryptvg/root \
  -u
cp /boot/initrd.gz $keydrvie

# Copy new kernel to the keydrive
if hash sbsign > /dev/null 2>&1 && [[ -f $dbkey && -f $dbcrt ]]; then
  sbsign --key $dbkey --cert $dbcrt \
    --output $keydrvie/vmlinuz-signed /boot/vmlinuz
else
  cp /boot/vmlinuz $keydrvie
fi

rm -rf /boot/initrd-tree
